#!/usr/bin/env python3

import os
import sys

import re
import requests
import pprint



def usage(status=0):
    ''' Display usage information and exit with specified status '''
    print('''Usage: {} [options] CITY STATE
        
        -v      give verbose crime information
        '''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)

def findCrimeRate(siteText, crimeType):
    regexSearch = "" + crimeType + "</td><td>[0-9]+,?[0-9]*\.?[0-9]*</td><td>n/a</td><td>[0-9]+,?[0-9]*\.?[0-9]*"
    crimeFound = re.findall(regexSearch, siteText)
    if not crimeFound:
        return "NA"
    crimeFound = re.split("</td><td>n/a</td><td>", crimeFound[0])
    return crimeFound[-1]

def findCrimeCategoryRate(siteText, crimeCategory):
    regexSearch = "" + crimeCategory + "</td><td>[0-9]+,?[0-9]*\.?[0-9]*</td><td>n/a<small> \(estimate\)</small></td><td>[0-9]+,?[0-9]*\.?[0-9]*"
    crimeFound = re.findall(regexSearch, siteText)
    if not crimeFound:
        regexSearch = "" + crimeCategory + "</td><td>n/a</td><td>n/a<small> \(estimate\)</small></td><td>[0-9]+,?[0-9]*\.?[0-9]*"
        crimeFound = re.findall(regexSearch, siteText)
    if not crimeFound:
        return "NA"
    crimeFound = re.split("</small></td><td>", crimeFound[0])
    return crimeFound[-1]




def getURL(location):
    locationParts = re.split(" ", location)
    del locationParts[-1]
    state = locationParts[-1]
    del locationParts[-1]
    
    #url to access
    url = "https://www.areavibes.com/"
    
    if len(locationParts) > 1:
        i = 0
        url = url + locationParts[0]
        i = i + 1
        while i < len(locationParts):
            url = url + "+" + locationParts[i]
            i = i + 1
    else:
        url = url + locationParts[0]

    url = url + "-"
    url = url + state + "/crime/"
    return url

def getURL(city, stateAbbrev):
    citywords = re.split(" ", city)
    #url to access
    url = "https://www.areavibes.com/"
    
    i = 1
    for word in citywords:
        url = url + word.lower()
        if i < len(citywords):
            url = url + "+"
        i = i + 1
    
    url = url + "-"
    url = url + stateAbbrev.lower() + "/crime/"
    return url

def getcrimeStatement(city, stateAbbrev, verbose):
    
    crimeStatement = ""
    
    murders = 0
    rapes = 0
    robberies = 0
    vcrime = 0
    pcrime = 0
    year = "2018"

    url = getURL(city, stateAbbrev)
    

    headers ={'user-agent':__name__}
    siteHTML = requests.get(url, headers=headers)
    siteText = siteHTML.text
    
    
    
    pcrime = findCrimeCategoryRate(siteText, "Property crime")
    crimeStatement = crimeStatement + "There was a Property Crime rate of " + pcrime + " per 100k people in " + year + "\n"
    
    vcrime = findCrimeCategoryRate(siteText, "Violent crime")
    crimeStatement = crimeStatement + "There was a Violent Crime rate of " + vcrime + " per 100k people in " + year + "\n"
    
    if verbose:
        murders = findCrimeRate(siteText, "Murder")
        if murders == "NA":
            crimeStatement = crimeStatement + "No Murder rate information available for this city\n"
        else:
            crimeStatement = crimeStatement + "There was a Murder rate of " + murders + " per 100k people in " + year + "\n"
        
        rapes = findCrimeRate(siteText, "Rape")
        if rapes == "NA":
            crimeStatement = crimeStatement + "No Rape rate information available for this city\n"
        else:
            crimeStatement = crimeStatement + "There was a Rape rate of " + rapes + " per 100k people in " + year + "\n"
        
        robberies = findCrimeRate(siteText, "Robbery")
        if robberies == "NA":
            crimeStatement = crimeStatement + "No Robbery rate information available for this city.\n"
        else:
            crimeStatement = crimeStatement + "There was a Robbery rate of " + robberies + " per 100k people in " + year + "\n"
    
    return crimeStatement







def main():
    arguments = sys.argv[1:]
    murders = 0
    rapes = 0
    robberies = 0
    vcrime = 0
    pcrime = 0
    year = "2018"
    locationArgs = ""
    verbose = False
    
    
    #parameter parsing
    if not arguments:
        usage(0)
        sys.exit(0)
    
    while len(arguments) and arguments[0].startswith('-'):
        argument = arguments.pop(0)
        if argument == '-h':
            usage(0)
            sys.exit(0)
        elif argument == '-v':
            verbose = True

    while len(arguments):
        locationArgs = locationArgs + arguments.pop(0) + " "
           
    url = getURL(locationArgs)


    
    
    #creating a file to write html code to
    #f = open("crimerateHTML.txt", "w")
    
    #gets website url
    headers ={'user-agent':__name__}
    siteHTML = requests.get(url, headers=headers)
    siteText = siteHTML.text
    
    #writes to file
    #f.write(siteText);

    pcrime = findCrimeCategoryRate(siteText, "Property crime")
    print("There was a Property Crime rate of", pcrime, "per 100k people in", year)

    vcrime = findCrimeCategoryRate(siteText, "Violent crime")
    print("There was a Violent Crime rate of", vcrime, "per 100k people in", year)

    if verbose:
        murders = findCrimeRate(siteText, "Murder")
        print("There was a Murder rate of", murders, "per 100k people in", year)
        
        rapes = findCrimeRate(siteText, "Rape")
        print("There was a Rape rate of", rapes, "per 100k people in", year)
        
        robberies = findCrimeRate(siteText, "Robbery")
        print("There was a Robbery rate of", robberies, "per 100k people in", year)

    #closes file
    #f.close()


# Main Execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:

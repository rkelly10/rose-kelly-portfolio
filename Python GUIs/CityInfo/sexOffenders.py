#!/usr/bin/env python3

import os
import sys

import re
import requests
import pprint



def usage(status=0):
    ''' Display usage information and exit with specified status '''
    print('''Usage: {} [options] CITY STATE
        
        -v      give verbose crime information
        '''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)

class SexOffender:
    def __init__(self, traitTuple, names, addresses):
        self.traitArr = ["number", "Sex", "Age", "Date of Birth", "Eye Color", "Hair Color", "Height", "Weight", "Race"]
        self.traitTuple = traitTuple
        self.name = names[int(traitTuple[0])]
        self.address = addresses[int(traitTuple[0])]

    def printSO(x):
        returnVal = x.name + "\n"
        returnVal = returnVal + "    Address: " + x.address + "\n"
        i = 0
        for t in x.traitTuple:
            if((t is not "") and (t is not "Unknown") and (x.traitArr[i] is not "number")):
                returnVal = returnVal + "    " + x.traitArr[i] + ": " + t + "\n"
            i = i + 1
        return returnVal



def findSexOffenderRatio(siteText):
    regexSearch = "<br><b>The ratio of all residents to sex offenders in [A-Za-z\s]+ is <\/b> [0-9]+,?[0-9]*\.?[0-9]*"
    sexOffenderRatioFound = re.findall(regexSearch, siteText)
    try:
        sexOffenderRatioFound = re.split("</b> ", sexOffenderRatioFound[0])
        return sexOffenderRatioFound[-1]
    except (IndexError, IndexError, TypeError):
        return "ERR"

def getLocalSexOffenders(zipcode, city, stateFull):
    sos = []
    url = getURL(city, stateFull)
    
    #gets website url
    headers ={'user-agent':__name__}
    siteHTML = requests.get(url, headers=headers)
    siteText = siteHTML.text
    
    returnVal = ""
    if((zipcode is "")):
        return "Please enter valid zipcode"
    #makes array of sex offender addresses
    regexSearch = "var addrTab = \[.*\]"
    addressesFound = re.findall(regexSearch, siteText)
    try:
        addressesFound = re.split("\",\"", addressesFound[0])
        addressesFound[0] = addressesFound[0].replace("var addrTab = [\"", "")
        addressesFound[-1] = addressesFound[-1].replace("\"]", "")
    except (IndexError, IndexError, TypeError):
        return "No information on sex offender populations for this city"

    #makes array of sex offender names
    regexSearch = "var nameTab = \[.*\]"
    namesFound = re.findall(regexSearch, siteText)
    try:
        namesFound = re.split("\",\"", namesFound[0])
        namesFound[0] = namesFound[0].replace("var nameTab = [\"", "")
        namesFound[-1] = namesFound[-1].replace("\"]", "")
    except (IndexError, IndexError, TypeError):
        return "No information on sex offender populations for this city"

    
    regexSearch = "<div class=\'(?:even|odd)\' id=\'[a-zA-Z0-9]+\'><strong><span id=\'sn([0-9]+)\'>(?:</span></strong><div id=\'so[0-9]+\'>)?.*<b>Address: </b><span id=\'sa[0-9]+\'></span>.*<b>Zip Code: </b>" + zipcode + "<br><b>Sex: </b>([a-zA-Z]+)<br><b>(?:Age: </b>([0-9]+)<br><b>)?(?:Date of birth: </b>([0-9-]+)<br><b>)?(?:Eye color: </b>([a-zA-Z]+)<br><b>)?(?:Hair color: </b>([a-zA-Z]+)<br><b>)?(?:Height: </b>([0-9]+\'[0-9]+)\"<br><b>)?(?:Weight: </b> ([0-9]+ lbs\.)<br><b>)?(?:Race: </b>([a-zA-Z \.]+))?"
    sexOffendersFound = re.findall(regexSearch, siteText)
    for p in sexOffendersFound:
        so = SexOffender(p, namesFound, addressesFound)
        sos.append(so)
        print(so.printSO())
        returnVal = so.printSO() + "\n"

    return sos



def getURL(location):
    locationParts = re.split(" ", location)
    del locationParts[-1]
    state = locationParts[-1]
    del locationParts[-1]
    
    #url to access
    url = "https://www.city-data.com/so/so-"
    
    #adding city words to url
    if len(locationParts) > 1:
        i = 0
        url = url + locationParts[0]
        i = i + 1
        while i < len(locationParts):
            url = url + "-" + locationParts[i]
            i = i + 1
    else:
        url = url + locationParts[0]
    
    #NOTE this one needs state to be entire word
    url = url + "-"
    url = url + state + ".html"
    return url

def getURL(city, stateFull):
    city = city.title()
    stateFull = stateFull.title()
    
    citywords = re.split(" ", city)
    statewords = re.split(" ", stateFull)
    
    #url to access
    url = "https://www.city-data.com/so/so-"
    
    i = 1
    for word in citywords:
        url = url + word
        url = url + "-"
    
    i = 1
    for word in statewords:
        url = url + word
        if i < len(statewords):
            url = url + "-"
        i = i + 1
    
    url = url + ".html"
    return url

def getSexOffenderStatement(city, stateFull):
    statement = ""
    url = getURL(city, stateFull)
    
    #gets website url
    headers ={'user-agent':__name__}
    siteHTML = requests.get(url, headers=headers)
    siteText = siteHTML.text
    
    sexOffenderRatio = findSexOffenderRatio(siteText)
    if sexOffenderRatio == "ERR":
        return "No information on sex offender populations for this city"
    else:
        statement = statement + "The ratio of residents to sex offenders is " + sexOffenderRatio + " residents for every 1 sex offender" + "\n"


    return statement



def main():
    arguments = sys.argv[1:]
    year = "2018"
    locationArgs = ""
    verbose = False
    
    #parameter parsing
    if not arguments:
        usage(0)
        sys.exit(0)
    
    while len(arguments) and arguments[0].startswith('-'):
        argument = arguments.pop(0)
        if argument == '-h':
            usage(0)
            sys.exit(0)
        elif argument == '-v':
            verbose = True

    while len(arguments):
        locationArgs = locationArgs + arguments.pop(0) + " "
    
    url = getURL("bethesda", "maryland")
    
    
    #creating a file to write html code to
    f = open("sexOfndrsHTML.txt", "w")
    
    #gets website url
    headers ={'user-agent':__name__}
    siteHTML = requests.get(url, headers=headers)
    siteText = siteHTML.text
    
    getLocalSexOffenders("[0-9]+", siteText)


    
    #writes to file
    f.write(siteText);
    
    #closes file
    f.close()


# Main Execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:


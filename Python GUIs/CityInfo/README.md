## How to Run the Program

After downloading the directory, cd in your terminal (or Putty) to the CityInfo folder.

Then use the command.   $ python3 cityInfoGUI2.py    to start the program. The GUI should boot up after a few seconds.

From there you can enter any city in the United States and the state it is located in to find information someone moving their may want to know including annual weather patterns, crime rates, and a list of sex offenders in the area.

![GUI Interface][gui1]

[gui1]: img/cityinfo_gui.png "GUI Interface"
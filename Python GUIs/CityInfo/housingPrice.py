#!/usr/bin/env python3

import os
import sys

import re
import requests
import pprint



def usage(status=0):
    ''' Display usage information and exit with specified status '''
    print('''Usage: {} [options] CITY STATE
        
        -v      give verbose crime information
        '''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)

def findHomePrice(siteText, searchType):
    regexSearch = "" + searchType + "</td><td>\$[0-9]+,?[0-9]*\.?[0-9]*"
    priceFound = re.findall(regexSearch, siteText)
    if not priceFound:
        return "NA"
    priceFound = re.split("</td><td>", priceFound[0])
    return priceFound[-1]


def getURL(location):
    locationParts = re.split(" ", location)
    del locationParts[-1]
    state = locationParts[-1]
    del locationParts[-1]
    
    #url to access
    url = "https://www.areavibes.com/"
    
    if len(locationParts) > 1:
        i = 0
        url = url + locationParts[0]
        i = i + 1
        while i < len(locationParts):
            url = url + "+" + locationParts[i]
            i = i + 1
    else:
        url = url + locationParts[0]

    url = url + "-"
    url = url + state + "/real-estate/"
    return url

def getURL(city, stateAbbrev):
    citywords = re.split(" ", city)
    #url to access
    url = "https://www.areavibes.com/"
    
    i = 1
    for word in citywords:
        url = url + word.lower()
        if i < len(citywords):
            url = url + "+"
        i = i + 1
    
    url = url + "-"
    url = url + stateAbbrev.lower() + "/real-estate/"
    return url

def getHousingStatement(city, stateAbbrev):
    
    housingStatement = ""
    
    homeprice = ""
    rent = ""

    url = getURL(city, stateAbbrev)
    

    headers ={'user-agent':__name__}
    siteHTML = requests.get(url, headers=headers)
    siteText = siteHTML.text
    
    
    
    homeprice = findHomePrice(siteText, "Median\shome\sprice")
    housingStatement = housingStatement + "Median Home Price: " + homeprice + "\n"
    
    rent = findHomePrice(siteText, "Median\srent\sasked")
    housingStatement = housingStatement + "Median Rent: " + rent + "\n"
    
    
    
    return housingStatement







def main():
    arguments = sys.argv[1:]
    murders = 0
    rapes = 0
    robberies = 0
    vcrime = 0
    pcrime = 0
    year = "2018"
    locationArgs = ""
    verbose = False
    
    
    #parameter parsing
    if not arguments:
        usage(0)
        sys.exit(0)
    
    while len(arguments) and arguments[0].startswith('-'):
        argument = arguments.pop(0)
        if argument == '-h':
            usage(0)
            sys.exit(0)
        elif argument == '-v':
            verbose = True

    while len(arguments):
        locationArgs = locationArgs + arguments.pop(0) + " "
           
           #url = getURL(locationArgs)


    
    url = getURL("south bend", "in")
    
    #creating a file to write html code to
    #f = open("crimerateHTML.txt", "w")
    
    #gets website url
    headers ={'user-agent':__name__}
    siteHTML = requests.get(url, headers=headers)
    siteText = siteHTML.text
    print(siteText)
    #writes to file
    #f.write(siteText);

    print(findHomePrice(siteText, "Median\shome\sprice"))
    print(findHomePrice(siteText, "Median\srent\sasked"))

    #closes file
    #f.close()


# Main Execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:

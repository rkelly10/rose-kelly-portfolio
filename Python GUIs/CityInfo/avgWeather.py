#!/usr/bin/env python3

import os
import sys

import re
import requests
import pprint



def usage(status=0):
    ''' Display usage information and exit with specified status '''
    print('''Usage: {} [options] ZIPCODE
        
        -v      give verbose weather information
        '''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)


def getURL(city, stateFull):
    city = city.lower()
    stateFull = stateFull.lower()
    
    citywords = re.split(" ", city)
    statewords = re.split(" ", stateFull)
    
    #url to access
    url = "https://www.usclimatedata.com/climate/"
    
    i = 1
    for word in citywords:
        url = url + word
        if i < len(citywords):
            url = url + "-"
        i = i + 1
    
    url = url + "/"
    
    i = 1
    for word in statewords:
        url = url + word
        if i < len(statewords):
            url = url + "-"
        i = i + 1
    
    url = url + "/united-states/"

    return url

def findTempHighs(siteText, tempType):
    monthlyTemps = {}
    regexSearch = ""
    splitText = ""
    cutoffLen = 48
    if tempType == "high":
        regexSearch = "Average high in ºF </span><span class=\"d-none d-sm-block d-lg-none\">Av\. high </span><span class=\"d-block d-sm-none\">Hi </span></td><td class=\"high text-right\">[0-9]+\.?[0-9]*</td><td class=\"high text-right\">[0-9]+\.?[0-9]*</td><td class=\"high text-right\">[0-9]+\.?[0-9]*</td><td class=\"high text-right\">[0-9]+\.?[0-9]*</td><td class=\"high text-right\">[0-9]+\.?[0-9]*</td><td class=\"high text-right\">[0-9]+\.?[0-9]*"
        splitText = "</td><td class=\"high text-right\">"
    elif tempType == "low":
        regexSearch = "Average low in ºF </span><span class=\"d-none d-sm-block d-lg-none\">Av\. low </span><span class=\"d-block d-sm-none\">Lo </span></td><td class=\"low text-right\">[0-9]+\.?[0-9]*</td><td class=\"low text-right\">[0-9]+\.?[0-9]*</td><td class=\"low text-right\">[0-9]+\.?[0-9]*</td><td class=\"low text-right\">[0-9]+\.?[0-9]*</td><td class=\"low text-right\">[0-9]+\.?[0-9]*</td><td class=\"low text-right\">[0-9]+\.?[0-9]*"
        splitText = "</td><td class=\"low text-right\">"
        cutoffLen = 47
    else:
        return monthlyTemps
    tempFound = re.findall(regexSearch, siteText)
    try:
        tempFound1 = re.split(splitText, tempFound[0])
        monthlyTemps["Jun"] = float(tempFound1[-1])
        tempFound2 = re.split(splitText, tempFound[1])
        monthlyTemps["Dec"] = float(tempFound2[-1])
    except (IndexError, TypeError):
        return monthlyTemps

    i = 1;
    while(i <= 5):
        
        regexSearch = regexSearch[0: int(len(regexSearch) - cutoffLen)]
        tempFound = re.findall(regexSearch, siteText)
        tempFound1 = re.split(splitText, tempFound[0])
        if i == 1:
            monthlyTemps["May"] = float(tempFound1[-1])
        elif i == 2:
            monthlyTemps["Apr"] = float(tempFound1[-1])
        elif i == 3:
            monthlyTemps["Mar"] = float(tempFound1[-1])
        elif i == 4:
            monthlyTemps["Feb"] = float(tempFound1[-1])
        elif i == 5:
            monthlyTemps["Jan"] = float(tempFound1[-1])
        tempFound2 = re.split(splitText, tempFound[1])
        if i == 1:
            monthlyTemps["Nov"] = float(tempFound2[-1])
        elif i == 2:
            monthlyTemps["Oct"] = float(tempFound2[-1])
        elif i == 3:
            monthlyTemps["Sep"] = float(tempFound2[-1])
        elif i == 4:
            monthlyTemps["Aug"] = float(tempFound2[-1])
        elif i == 5:
            monthlyTemps["Jul"] = float(tempFound2[-1])
        i = i + 1

    return monthlyTemps





def findYearlyRain(siteText):
    regexSearch = "Average annual precip.</td><td>[0-9]+\.?[0-9]* inch"
    rainFound = re.findall(regexSearch, siteText)
    try:
        rainFound = re.split("</td><td>", rainFound[-1])
        rainFound = rainFound[-1]
    except (IndexError, TypeError):
        return ""
    retStr = "Average Annual Rainfall: " + rainFound + "\n"
    return retStr



def get3MonthAvg(siteText, m1, m2, m3):
    temp = 0
    temp = temp + findAvgTemp(m1, siteText) + findAvgTemp(m2, siteText) + findAvgTemp(m3, siteText)
    avgTemp = temp / 3
    return avgTemp

def getSeasonAvg(siteText, season, highTemps, lowTemps):
    if (not highTemps) or (not lowTemps):
        return "error"
    ht = 0
    lt = 0
    if season == "Winter":
        ht = (highTemps["Dec"] + highTemps["Jan"] + highTemps["Feb"]) / 3
        lt = (lowTemps["Dec"] + lowTemps["Jan"] + lowTemps["Feb"]) / 3
    elif season == "Spring":
        ht = (highTemps["Mar"] + highTemps["Apr"] + highTemps["May"]) / 3
        lt = (lowTemps["Mar"] + lowTemps["Apr"] + lowTemps["May"]) / 3
    elif season == "Summer":
        ht = (highTemps["Jun"] + highTemps["Jul"] + highTemps["Aug"]) / 3
        lt = (lowTemps["Jun"] + lowTemps["Jul"] + lowTemps["Aug"]) / 3
    elif season == "Autumn":
        ht = (highTemps["Sep"] + highTemps["Oct"] + highTemps["Nov"]) / 3
        lt = (lowTemps["Sep"] + lowTemps["Oct"] + lowTemps["Nov"]) / 3

    stmnt = season + " average temperature range: {:0.2f}ºF.".format(lt) + " - {:0.2f}ºF.".format(ht) + "\n"
    return stmnt


def getWeatherStatement(city, stateFull):
    weatherStatement = ""
    url = getURL(city.lower(), stateFull.lower())
    headers ={'user-agent':__name__}
    siteHTML = requests.get(url, headers=headers)
    siteText = siteHTML.text
    
    ht = findTempHighs(siteText, "high")
    
    lt = findTempHighs(siteText, "low")
    
    
    if not ht:
        weatherStatement = weatherStatement + "No annual temperature information available for this city\n"
    else:
        weatherStatement = weatherStatement + getSeasonAvg(siteText, "Autumn", ht, lt)
        weatherStatement = weatherStatement + getSeasonAvg(siteText, "Winter", ht, lt)
        weatherStatement = weatherStatement + getSeasonAvg(siteText, "Spring", ht, lt)
        weatherStatement = weatherStatement + getSeasonAvg(siteText, "Summer", ht, lt)

    raintest = findYearlyRain(siteText)
    if not raintest:
        weatherStatement = weatherStatement + "No annual rainfall information available for this city\n"
    else:
        weatherStatement = weatherStatement + raintest

    return weatherStatement


def main():
    arguments = sys.argv[1:]
    zip = ""
    verbose = False
    
    #parameter parsing
    if not arguments:
        usage(0)
        sys.exit(0)
    
    while len(arguments) and arguments[0].startswith('-'):
        argument = arguments.pop(0)
        if argument == '-h':
            usage(0)
            sys.exit(0)
        elif argument == '-v':
            verbose = True

    zip = arguments.pop(0)
    
    url = getURL("new yorK", "new yorK")
    
    
    
    
    #creating a file to write html code to
    #f = open("weatherHTML.txt", "w")
    
    #gets website url
    headers ={'user-agent':__name__}
    siteHTML = requests.get(url, headers=headers)
    siteText = siteHTML.text

    ht = findTempHighs(siteText, "high")
    print(ht)
    lt = findTempHighs(siteText, "low")
    print(lt)
    print(findYearlyRain(siteText))


    print(getWeatherStatement("new yorK", "new yorK"))
    

    #print("Autumn average temperature: {:0.2f}ºF.".format(get3MonthAvg(siteText, "September", "October", "November")))

    #print("Winter average temperature: {:0.2f}ºF.".format(get3MonthAvg(siteText, "December", "January", "February")))

    #print("Spring average temperature: {:0.2f}ºF.".format(get3MonthAvg(siteText, "March", "April", "May")))

    #print("Summer average temperature: {:0.2f}ºF.".format(get3MonthAvg(siteText, "June", "July", "August")))

    #print("Yearly rainfall total: {:0.1f} inches.".format(findYearlyRain(siteText)))
    
    #writes to file
    #f.write(siteText);


    #closes file
    #f.close()


# Main Execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:

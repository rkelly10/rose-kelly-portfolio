#!/usr/bin/env python3

import PySimpleGUI as sg
import locationInputTranslation as lt

from avgWeather import getWeatherStatement
from crimeRate import getcrimeStatement
from housingPrice import getHousingStatement
from sexOffenders import getSexOffenderStatement
from sexOffenders import getLocalSexOffenders

sg.theme('GreenTan')

layout = [[sg.Text('Enter city and state you would like to look up information for.', size=(50, 1), font=("Helvetica", 25))],
          [sg.Text('City: '), sg.Input(key='-city-'), sg.Text('State: '), sg.Input(key='-state-'), sg.Text('Zip Code (optional): '), sg.Input(key='-zipcode-')],
          [sg.Checkbox('Recieve verbose crime information',key='-verboseCrime-')],
          [sg.Text('_'  * 180)],
          [sg.Button('Show'), sg.Button('Exit'),],
          [sg.Text('Annual Weather:'), sg.Text(size=(80,6), key='-weather-')],
          [sg.Text('Housing Pricing:'), sg.Text(size=(80,3), key='-housing-')],
          [sg.Text('Crime Rate:'), sg.Text(size=(80,6), key='-crime-')],
          [sg.Text('Sex Offender Population:'), sg.Text(size=(80,3), key='-sexOffender-')],
          [sg.Text('Sex Offenders in your Zipcode:'), sg.Output(size=(50,10), key='-sexOffenderInfo-') ],
          [sg.Text(' ')],
          [sg.Text(''), sg.Text(size=(80,4), key='-error-')],
          [sg.Text(' ' * 235 + 'Resources used include areavibes.com, usclimatedata.com, and city-data.com'), sg.Text(text_color='white', key='-resources-')]   ]

window = sg.Window('City Information Lookup', layout)

while True:  # Event Loop
    event, values = window.read()
    print(event, values)
    if event == sg.WIN_CLOSED or event == 'Exit':
        break
    if event == 'Show':
        # Update the "output" text element to be the value of "input" element
        city = values['-city-']
        stateAbrv = ""
        stateFull = ""
        zipcode = ""
        validInput = True
        
        if lt.isValidStateFull(values['-state-']):
            stateFull = values['-state-']
            stateAbrv = lt.getStateAbbreviation(values['-state-'])
        elif lt.isValidStateAbbr(values['-state-']):
            stateAbrv = values['-state-']
            stateFull = lt.getStateFull(values['-state-'])
        else:
            validInput = False

        #state is valid
        if validInput:
            zipcode = lt.getZip(city, stateAbrv)
            soPopulations = getSexOffenderStatement(city, stateFull)
            if (zipcode == "ERR") or (soPopulations == "No information on sex offender populations for this city"):
                validInput = False
            
            if values['-zipcode-']:
                zipcode = values['-zipcode-']
            else:
                zipcode = "[0-9]+"

            #city is valid
            if validInput:
                window['-crime-'].update(getcrimeStatement(city, stateAbrv, values["-verboseCrime-"]))
                window['-housing-'].update(getHousingStatement(city, stateAbrv))
                window['-sexOffender-'].update(soPopulations)
                window['-weather-'].update(getWeatherStatement(city, stateFull))
                sos = getLocalSexOffenders(zipcode, city, stateFull)
                if(type(sos) is str):
                    window['-sexOffenderInfo-'].update('')
                    print(sos)
                else:
                    window['-sexOffenderInfo-'].update('')
                    for s in sos:
                        print(s.printSO())
                window['-error-'].update("")
            else:
                window['-error-'].update("ERROR: city input invalid, please enter valid city name")
                window['-crime-'].update("")
                window['-housing-'].update("")
                window['-sexOffender-'].update("")
                window['-weather-'].update("")
                window['-sexOffenderInfo-'].update('')

        else:
            window['-error-'].update("ERROR: state input invalid, please enter valid state name or state abbreviation")
            window['-crime-'].update("")
            window['-housing-'].update("")
            window['-sexOffender-'].update("")
            window['-weather-'].update("")
            window['-sexOffenderInfo-'].update('')


window.close()

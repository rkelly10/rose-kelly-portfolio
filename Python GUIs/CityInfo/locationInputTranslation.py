#!/usr/bin/env python3

import os
import sys

import re
import requests
import pprint

from avgWeather import getWeatherStatement
from crimeRate import getcrimeStatement
from sexOffenders import getSexOffenderStatement

stateToAbbrDict = {
    "alabama" : "AL",
    "alaska" : "AK",
    "arizona" : "AZ",
    "arkansas" : "AR",
    "california" :"CA",
    "colorado" : "CO",
    "connecticut" : "CT",
    "delaware" : "DE",
    "district of columbia" : "DC",
    "florida" : "FL",
    "georgia" : "GA",
    "hawaii" : "HI",
    "idaho" : "ID",
    "illinois" : "IL",
    "indiana" : "IN",
    "iowa" : "IA",
    "kansas" : "KS",
    "kentucky" : "KY",
    "louisiana" : "LA",
    "maine" : "ME",
    "maryland" : "MD",
    "massachusetts" : "MA",
    "michigan" : "MI",
    "minnesota" : "MN",
    "mississippi" : "MS",
    "missouri" : "MO",
    "montana" : "MT",
    "nebraska" : "NE",
    "nevada" : "NV",
    "new hampshire" : "NH",
    "new jersey" : "NJ",
    "new mexico" : "NM",
    "new york" : "NY",
    "north carolina" : "NC",
    "north dakota" : "ND",
    "ohio" : "OH",
    "oklahoma" : "OK",
    "oregon"    : "OR",
    "pennsylvania" : "PA",
    "rhode island" : "RI",
    "south carolina" : "SC",
    "south dakota" : "SD",
    "tennessee" : "TN",
    "texas" : "TX",
    "utah" : "UT",
    "vermont" : "VT",
    "virginia" : "VA",
    "washington" : "WA",
    "west virginia" : "WV",
    "wisconsin" : "WI",
    "wyoming" : "WY"
}

abbrToStateDict = {
    "AL" : "Alabama",
    "AK" : "Alaska",
    "AZ" : "Arizona",
    "AR" : "Arkansas",
    "CA" : "California",
    "CO" : "Colorado",
    "CT" : "Connecticut",
    "DE" : "Delaware",
    "DC" : "District of Columbia",
    "FL" : "Florida",
    "GA" : "Georgia",
    "HI" : "Hawaii",
    "ID" : "Idaho",
    "IL" : "Illinois",
    "IN" : "Indiana",
    "IA" : "Iowa",
    "KS" : "Kansas",
    "KY" : "Kentucky",
    "LA" : "Louisiana",
    "ME" : "Maine",
    "MD" : "Maryland",
    "MA" : "Massachusetts",
    "MI" : "Michigan",
    "MN" : "Minnesota",
    "MS" : "Mississippi",
    "MO" : "Missouri",
    "MT" : "Montana",
    "NE" : "Nebraska",
    "NV" : "Nevada",
    "NH" : "New Hampshire",
    "NJ" : "New Jersey",
    "NM" : "New Mexico",
    "NY" : "New York",
    "NC" : "North Carolina",
    "ND" : "North Dakota",
    "OH" : "Ohio",
    "OK" : "Oklahoma",
    "OR" : "Oregon",
    "PA" : "Pennsylvania",
    "RI" : "Rhode Island",
    "SC" : "South Carolina",
    "SD" : "South Dakota",
    "TN" : "Tennessee",
    "TX" : "Texas",
    "UT" : "Utah",
    "VT" : "Vermont",
    "VA" : "Virginia",
    "WA" : "Washington",
    "WV" : "West Virginia",
    "WI" : "Wisconsin",
    "WY" : "Wyoming"
}



def usage(status=0):
    ''' Display usage information and exit with specified status '''
    print('''Usage: {} [options] CITY STATE
        
        -v      give verbose crime information
        '''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)

#pass entire state name as parameter to get abbreviation
def getStateAbbreviation(stateFull):
    return stateToAbbrDict.get(stateFull.lower())

#pass abbreviation as parameter to get entire state name
def getStateFull(stateAbbr):
    return abbrToStateDict.get(stateAbbr.upper())


def isValidStateFull(stateFull):
    if(stateToAbbrDict.get(stateFull.lower()) == None):
        return False
    else:
        return True

def isValidStateAbbr(stateAbbr):
    if(abbrToStateDict.get(stateAbbr.upper()) == None):
        return False
    else:
        return True


def getZip(city, stateAbbr):
    citywords = re.split(" ", city)
    
    #url to access
    #https://www.zip-codes.com/search.asp?fld-city=South+Bend&fld-state=IN&selectTab=2&Submit=Find+ZIP+Codes
    url = "https://www.zip-codes.com/search.asp?fld-city="
    
    i = 1
    for word in citywords:
        url = url + word
        if i < len(citywords):
            url = url + "+"
        i = i + 1
    
    url = url + "&fld-state="
    url = url + stateAbbr + "&selectTab=2&Submit=Find+ZIP+Codes"

    #gets website url
    headers ={'user-agent':__name__}
    siteHTML = requests.get(url, headers=headers)
    siteText = siteHTML.text
    
    regexSearch = "<td class=a><a href=\"/zip-code/[0-9]+"
    zipFound = re.search(regexSearch, siteText)
    try:
        zipFound = re.split("/zip-code/", zipFound[0])
        return zipFound[-1]
    except (IndexError, IndexError, TypeError):
        return "ERR"


def main():
    arguments = sys.argv[1:]
    year = "2018"
    locationArgs = ""
    verbose = False
    
    #parameter parsing
    if not arguments:
        usage(0)
        sys.exit(0)
    
    while len(arguments) and arguments[0].startswith('-'):
        argument = arguments.pop(0)
        if argument == '-h':
            usage(0)
            sys.exit(0)
        elif argument == '-v':
            verbose = True

    while len(arguments):
        locationArgs = locationArgs + arguments.pop(0) + " "
    
    abbreviation = getStateFull("Ny")
    print(abbreviation)
    
    x = stateToAbbrDict.get("georgia")
    print(x)
    
    
    zip = getZip("New York", "NY")
    #requires zipcode
    print(getWeatherStatement(zip))
    #requires city, state abbreviation
    print(getcrimeStatement("New York", "NY", True))
    #requires city state full
    print(getSexOffenderStatement("New York", "New York"))



# Main Execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:


import numpy as np

from bokeh.io import curdoc
from bokeh.layouts import column, row
from bokeh.models import ColumnDataSource, Slider, TextInput
from bokeh.plotting import figure

# Set up data

#main line
N = 2
x = np.linspace(0, 0.001, N)
y = np.linspace(0*300000, 15*300000, N)
source = ColumnDataSource(data=dict(x=x, y=y))

#end dot
dotsource = ColumnDataSource(data=dict(x=[0,0], y=[0*300000,15*300000]))

# perp lines
xlines = np.linspace(-30, 30, N)
ylines1 = np.linspace(2*300000, 2*300000, N)
ylines2 = np.linspace(6*300000, 6*300000, N)
ylines3 = np.linspace(10*300000, 10*300000, N)
linesource1 = ColumnDataSource(data=dict(x=xlines, y=ylines1))
linesource2 = ColumnDataSource(data=dict(x=xlines, y=ylines2))
linesource3 = ColumnDataSource(data=dict(x=xlines, y=ylines3))

# walls light bounces off of
wx1 = np.linspace(-2, -2.001, N)
wx2 = np.linspace(2, 2.001, N)
wy = np.linspace(-25*300000, 25*300000, N)
wallsrc1 = ColumnDataSource(data=dict(x=wx1, y=wy))
wallsrc2 = ColumnDataSource(data=dict(x=wx2, y=wy))

#light lines
lx1 = np.linspace(0, 2, N)
ly1 = lx1*300000
lxn1 = np.linspace(0, -2, N)
lyn1 = -lxn1*300000
lightsrc1 = ColumnDataSource(data=dict(x=lx1, y=ly1))
lightsrcn1 = ColumnDataSource(data=dict(x=lxn1, y=lyn1))

lx2 = np.linspace(-2, 2, N)
ly2 = 2*lx1*300000 + 2*300000
lxn2 = np.linspace(2, -2, N)
lyn2 = -2*lxn1*300000 + 2*300000
lightsrc2 = ColumnDataSource(data=dict(x=lx2, y=ly2))
lightsrcn2 = ColumnDataSource(data=dict(x=lxn2, y=lyn2))

lx3 = np.linspace(-2, 2, N)
ly3 = 2*lx1*300000 + 6*300000
lxn3 = np.linspace(2, -2, N)
lyn3 = -2*lxn1*300000 + 6*300000
lightsrc3 = ColumnDataSource(data=dict(x=lx3, y=ly3))
lightsrcn3 = ColumnDataSource(data=dict(x=lxn3, y=lyn3))

# Set up plot
plot = figure(plot_height=400, plot_width=400, title="Special Relativity Demo",
              x_axis_label='Time (seconds)', tools="crosshair,pan,reset,save,wheel_zoom",
              y_axis_label='Distance (Kilometers)', x_range=[-15, 15], y_range=[-2*300000, 6000000])

# main line
plot.line('x', 'y', source=source, line_width=3, line_color="black", line_alpha=0.6)

# end dots
plot.circle('x', 'y', source=dotsource, size=20, color="navy", alpha=0.5)

# perp lines
plot.line('x', 'y', source=linesource1, line_width=2, line_color="black", line_alpha=0.3)
plot.line('x', 'y', source=linesource2, line_width=2, line_color="black", line_alpha=0.3)
plot.line('x', 'y', source=linesource3, line_width=2, line_color="black", line_alpha=0.3)

# walls light bounces off of
plot.line('x', 'y', source=wallsrc1, line_width=3, line_alpha=0.6)
plot.line('x', 'y', source=wallsrc2, line_width=3, line_alpha=0.6)


#light lines
plot.line('x', 'y', source=lightsrc1, line_width=3, line_dash='dashed', line_color="orange", line_alpha=0.6)
plot.line('x', 'y', source=lightsrcn1, line_width=3, line_dash='dashed', line_color="orange", line_alpha=0.6)

plot.line('x', 'y', source=lightsrc2, line_width=3, line_dash='dashed', line_color="orange", line_alpha=0.6)
plot.line('x', 'y', source=lightsrcn2, line_width=3, line_dash='dashed', line_color="orange", line_alpha=0.6)

plot.line('x', 'y', source=lightsrc3, line_width=3, line_dash='dashed', line_color="orange", line_alpha=0.6)
plot.line('x', 'y', source=lightsrcn3, line_width=3, line_dash='dashed', line_color="orange", line_alpha=0.6)


# Set up widgets
xa = Slider(title="x1", value=0, start=-13.0, end=13.0, step=0.1)
ya = Slider(title="y1", value=-7.0, start=-13.0, end=13.0, step=0.1)
xb = Slider(title="Change speed", value=0, start=-20.0, end=20.0, step=0.1)
yb = Slider(title="y2", value=8.0, start=-13.0, end=13.0, step=0.1)


# Set up callbacks
def update_data(attrname, old, new):
    
    # Get the current slider values
    a = 0
    b = 0*300000
    c = xb.value
    d = 15*300000
    
    # Generate the new line
    slopemain = (d-b)/(c-a)
    x = np.linspace(a, c, N)
    y = ((d-b)/(c-a))*x + ( d-( ( (d-b)/(c-a) ) * c ) )
    
    #generate perpendicular lines
    linelength = np.sqrt((c-a)*(c-a) + (d-b)*(d-b))
    
    #calculate rise and run for length of 2 on line
    slope = 1 /(((d-b)/(c-a)))
    
    
    #light walls
    distance = 2 / (1/np.sqrt(1-slope*slope*300000*300000))
    w1_x_intrcpt = a - distance
    w2_x_intrcpt = a + distance
    wy1 = slopemain * xlines + (b - slopemain*w1_x_intrcpt)
    wy2 = slopemain * xlines + (b - slopemain*w2_x_intrcpt)
    
    #light lines
    lx1_endy = (b - slopemain*w2_x_intrcpt - (b + a))/(300000+slopemain)
    lx1 = np.linspace(a, lx1_endy, N)
    ly1 = -lx1*300000 + (b - a)
    lxn1_endy = (b - slopemain*w2_x_intrcpt - (b - a))/(300000-slopemain)
    lxn1 = np.linspace(lxn1_endy, a, N)
    lyn1 = lxn1*300000 + (b + a)
    
    
    lx2_beginy = lxn1_endy
    lx2_endy = ((b - slopemain*w1_x_intrcpt)-(2*lx2_beginy*300000 + b))/(-300000-slopemain)
    lx2 = np.linspace(lx2_beginy, lx2_endy, N)
    ly2 = -lx2*300000 + 2*lx2_beginy*300000 + b
    
    lxn2_beginy = lx1_endy
    lxn2_endy = ((b - slopemain*w2_x_intrcpt)-(-2*lxn2_beginy*300000 + b))/(300000-slopemain)
    lxn2 = np.linspace(lxn2_beginy, lxn2_endy, N)
    lyn2 = lxn2*300000 + b - 2*lxn2_beginy*300000
    
    lx3_beginy = lx2_endy
    lx3_endy = ((b - slopemain*w2_x_intrcpt)-(2*lx2_beginy*300000 - 2*lx3_beginy*300000 + b))/(300000-slopemain)
    lx3 = np.linspace(lx3_beginy, lx3_endy, N)
    ly3 = lx3*300000 + b + 2*lx2_beginy*300000 - 2*lx3_beginy*300000
    
    lxn3_beginy = lxn2_endy
    lxn3_endy = ((b - slopemain*w1_x_intrcpt)-(-2*lxn2_beginy*300000 + 2*lxn3_beginy*300000 + b))/(-300000 - slopemain)
    lxn3 = np.linspace(lxn3_beginy, lxn3_endy, N)
    lyn3 = -lxn3*300000 + b - 2*lxn2_beginy*300000 + 2*lxn3_beginy*300000
    
    
    
    #perp lines
    yl1 = (((lxn1_endy*300000-a+b)-(a-lx1_endy*300000+b))/(lxn1_endy-lx1_endy))*xlines + (lxn1_endy*300000-a+b) - (((lxn1_endy*300000-a+b)-(a-lx1_endy*300000+b))/(lxn1_endy-lx1_endy))*lxn1_endy
    
    yl2 = (((lxn2_endy*300000 + b - 2*lxn2_beginy*300000)-(-lx2_endy*300000 + 2*lx2_beginy*300000 + b))/(lxn2_endy-lx2_endy))*xlines + (lxn2_endy*300000 + b - 2*lxn2_beginy*300000) - (((lxn2_endy*300000 + b - 2*lxn2_beginy*300000)-(-lx2_endy*300000 + 2*lx2_beginy*300000 + b))/(lxn2_endy-lx2_endy))*lxn2_endy
    
    yl3 = (((-lxn3_endy*300000 + b - 2*lxn2_beginy*300000 + 2*lxn3_beginy*300000)-(lx3_endy*300000 + b + 2*lx2_beginy*300000 - 2*lx3_beginy*300000))/(lxn3_endy-lx3_endy))*xlines + (-lxn3_endy*300000 + b - 2*lxn2_beginy*300000 + 2*lxn3_beginy*300000) - (((-lxn3_endy*300000 + b - 2*lxn2_beginy*300000 + 2*lxn3_beginy*300000)-(lx3_endy*300000 + b + 2*lx2_beginy*300000 - 2*lx3_beginy*300000))/(lxn3_endy-lx3_endy))*lxn3_endy
    
    source.data = dict(x=x, y=y)
    dotsource.data = dict(x=[a,c], y=[b,d])
    linesource1.data = dict(x=xlines, y=yl1)
    linesource2.data = dict(x=xlines, y=yl2)
    linesource3.data = dict(x=xlines, y=yl3)
    wallsrc1.data = dict(x=xlines, y=wy1)
    wallsrc2.data = dict(x=xlines, y=wy2)
    lightsrc1.data = dict(x=lx1, y=ly1)
    lightsrcn1.data = dict(x=lxn1, y=lyn1)
    lightsrc2.data = dict(x=lx2, y=ly2)
    lightsrcn2.data = dict(x=lxn2, y=lyn2)
    lightsrc3.data = dict(x=lx3, y=ly3)
    lightsrcn3.data = dict(x=lxn3, y=lyn3)

for i in [xa, ya, xb, yb]:
    i.on_change('value', update_data)


# Set up layouts and add to document
inputs = column(xb)

curdoc().add_root(row(inputs, plot, width=800))
curdoc().title = "SpecialRelativity"

console.log("started script");

add_song_button = document.getElementById("add-song-button");
add_song_button.onmouseup = makePostRequest;

delete_song_button = document.getElementById("delete-song-button");
delete_song_button.onmouseup = makeDeleteRequest;

clear_button = document.getElementById("clear-button");
clear_button.onmouseup = makeClear;

display_button = document.getElementById("display-button");
display_button.onmouseup = makeGetRequest;

reset_button = document.getElementById("reset-button");
reset_button.onmouseup = resetPlaylist;

delete_button = document.getElementById("delete-button");
delete_button.onmouseup = deletePlaylist;

function getValuesFromForm(){
    //select dropdown select-server-address
    // var selidx = document.getElementById('select-server-address').selectedIndex;
    // var url_base = document.getElementById('select-server-address').options[selidx].value;

    var url_base = "http://student04.cse.nd.edu";

    var port_num = 51024

    var url = url_base + ":" + port_num + "/songs/";


    console.log(url);

    // text input-sname
    var sname = document.getElementById('input-sname').value;
    console.log("song name: " + sname);

    // text input-sartist
    var sartist = document.getElementById('input-sartist').value;
    console.log("artist name: " + sartist);

    // text input-syear
    var syear = document.getElementById('input-syear').value;
    console.log("release year: " + syear);

    var snum = document.getElementById('input-snum').value;
    console.log("song num: " + snum);


    //radio group - button1 id="radio-get", value="GET" id="radio-put"



    var message_body_json = {"title":sname, "artist":sartist, "year":syear};
    var message_body_str = JSON.stringify(message_body_json);


    var parameters = [url, message_body_str, snum];
    console.log(parameters);
    return parameters;
} // end of get Values from Form

function get_sort_type(){
    var sort_type = "by-id"; // default
    if (document.getElementById('alphabetical-sort').checked){
        sort_type = "alpha";
    } else if (document.getElementById('chronological-sort').checked) {
        sort_type = "chrono";
    } else if (document.getElementById('by-id-sort').checked) {
        sort_type = "by-id";
    }

    return sort_type;
}

function makeClear(){
    console.log("started make clear");
    document.getElementById('input-sname').value = "";
    document.getElementById('input-sartist').value = "";
    document.getElementById('input-syear').value = "";
    document.getElementById('answer-label').innerHTML = ""; //clear output to front end
} // end of makeClear

function resetPlaylist(){
    console.log("started reset playlist");

    var params = getValuesFromForm();
    var url = "http://student04.cse.nd.edu:51024/reset/";

    var xhr = new XMLHttpRequest();
    xhr.open("PUT", url, true);

    xhr.onload = function(e) {
			if (xhr.readyState === 4) {
				console.log(xhr.responseText);
                document.getElementById('answer-label').innerHTML = "";
                var full_response = JSON.parse(xhr.responseText);


			} else {
				console.error(xhr.statusText);
			}
		};

	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}
	xhr.send(params[1]);
}

function deletePlaylist(){
    console.log("started delete playlist");

    var params = getValuesFromForm();
    var url = "http://student04.cse.nd.edu:51024/songs/";

    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", url, true);

    xhr.onload = function(e) {
			if (xhr.readyState === 4) {
				console.log(xhr.responseText);
                document.getElementById('answer-label').innerHTML = "";
                var full_response = JSON.parse(xhr.responseText);


			} else {
				console.error(xhr.statusText);
			}
		};

	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}
	xhr.send(params[1]);
}

function makePostRequest(){
    // get values from form elements
    console.log("started post request!: ");

    var params = getValuesFromForm();

	var xhr = new XMLHttpRequest();
    xhr.open("POST", params[0], true);


	xhr.onload = function(e) {
			if (xhr.readyState === 4) {
				console.log(xhr.responseText);
                document.getElementById('answer-label').innerHTML = "";
                var full_response = JSON.parse(xhr.responseText);


			} else {
				console.error(xhr.statusText);
			}
		};

	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}
    console.log(params[1]);
	xhr.send(params[1]);
}

function makeDeleteRequest(){
    // get values from form elements
    console.log("started delete request!: ");

    var params = getValuesFromForm();

    var url_base = params[0];

    var url = url_base + params[2];

	var xhr = new XMLHttpRequest();
    xhr.open("DELETE", url, true);


	xhr.onload = function(e) {
			if (xhr.readyState === 4) {
				console.log(xhr.responseText);
                document.getElementById('answer-label').innerHTML = "";
                var full_response = JSON.parse(xhr.responseText);

			} else {
				console.error(xhr.statusText);
			}
		};

	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}
	xhr.send(params[1]);
}

function displaySorted(songs_list){
    var sort_type = get_sort_type();

    if(sort_type=="alpha"){
        songs_list.sort((a, b) => (a.title > b.title) ? 1 : -1)
    }
    else if(sort_type=="chrono"){
        songs_list.sort((a, b) => (a.year > b.year) ? 1 : -1)
    }
    var sname = "";
    var sartist = "";
    var syear = "";
    for (i = 0; i < songs_list.length; i++){
        sname = songs_list[i]['title'];
        sartist = songs_list[i]['artist'];
        syear = songs_list[i]['year'];
        snum = songs_list[i]['id'];
        document.getElementById('answer-label').innerHTML += "<p>"+snum+". " + sname + " is by " + sartist + " from " + syear + "</p>";
    }
}

function makeGetRequest(){
    // get values from form elements
    console.log("started get request!: ");
    // get networking data ready
    var url = "http://student04.cse.nd.edu:51024/";
    // make network call

    var params = getValuesFromForm();

	var xhr = new XMLHttpRequest();
    xhr.open("GET", params[0], true);


	xhr.onload = function(e) {
			if (xhr.readyState === 4) {
				console.log(xhr.responseText);
                document.getElementById('answer-label').innerHTML = "";
                var full_response = JSON.parse(xhr.responseText);
                var songs_list = full_response["songs"];
                displaySorted(songs_list);
                var sname = "";
                var sartist = "";
                var syear = "";
                /*for (i = 0; i < songs_list.length; i++){
                    sname = songs_list[i]['title'];
                    sartist = songs_list[i]['artist'];
                    syear = songs_list[i]['year'];
                    snum = songs_list[i]['id'];
                    document.getElementById('answer-label').innerHTML += "<p>"+snum+". " + sname + " is by " + sartist + " from " + syear + "</p>";
                }*/



			} else {
				console.error(xhr.statusText);
			}
		};

	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}
	xhr.send(params[1]);
} // end of makeRequest

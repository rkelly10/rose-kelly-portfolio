#Frank Gomulka(fgomulka)
#Gehrig Chao(gchao2)
#Rose Kelly(rkelly10)
import cherrypy
import re, json
import time
from songs_library import _song_database

class SongController(object):

        def __init__(self, sdb=None, am=None):
                if sdb is None:
                        self.sdb = _song_database()
                else:
                        self.sdb = sdb

                self.sdb.load_songs("songlist.json")

        def GET_KEY(self, song_id):
                output = {"result":"success"}
                song_id = int(song_id)

                time.sleep(5)

                try:
                        song = self.sdb.get_song(song_id)
                        if song is not None:
                                output["id"] = song_id
                                output["title"] = song[0]
                                output["artist"] = song[1]
                                output["year"] = song[2]
                        else:
                                output["result"] = "error"
                                output["message"] = "song not found"
                except Exception as ex:
                        output["result"] = "error"
                        output["message"] = str(ex)

                return json.dumps(output)

        def PUT_KEY(self, song_id):
                output = {"result":"success"}
                song_id = int(song_id)

                data = json.loads(cherrypy.request.body.read().decode("utf-8"))

                song = list()
                song.append(data["title"])
                song.append(data["artist"])
                song.append(data["year"])

                self.sdb.set_song(song_id, song)

                return json.dumps(output)

        def DELETE_KEY(self, song_id):
                #TODO
                output = {"result":"success"}
                song_id = int(song_id)
                time.sleep(5)
                try:
                      self.sdb.delete_song(song_id)
                except Exception as ex:
                      output["result"] = "error"
                      output["message"] = str(ex)
                return json.dumps(output)

        def GET_INDEX(self):
                output = {"result":"success"}
                output["songs"] = []

                try:
                        for sid in self.sdb.get_songs():
                               song = self.sdb.get_song(sid)
                               dsong = {"id":sid, "title":song[0],
                                                "artist":song[1],
                                                 "year":song[2]}
                               output["songs"].append(dsong)
                except Exception as ex:
                        output["result"] = "error"
                        output["message"] = str(ex)

                return json.dumps(output)

        def POST_INDEX(self):
                #TODO
                output = {"result":"success"}
                data = json.loads(cherrypy.request.body.read().decode("utf-8"))

                try:
                    song = list()
                    song.append(data["title"])
                    song.append(data["artist"])
                    song.append(data["year"])
                    # size = len(self.sdb.get_songs())
                    max = 0
                    for sid in self.sdb.get_songs():
                        if sid > max:
                            max = sid

                    sid = max+1
                    self.sdb.set_song(sid,song)
                    output["id"] = sid
                except Exception as ex:
                    output["result"] = "error"
                    output["message"] = str(ex)

                return json.dumps(output)

        def DELETE_INDEX(self):
          output = {"result":"success"}
          time.sleep(5)
          try:
            temp = list(self.sdb.get_songs())
            for sid in temp:
              song = self.sdb.delete_song(sid)
          except Exception as ex:
            output["result"] = "error"
            output["message"] = str(ex)
          return json.dumps(output)

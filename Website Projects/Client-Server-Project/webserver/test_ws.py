import unittest
import requests
import json

class TestAll(unittest.TestCase):

    SITE_URL = "http://student04.cse.nd.edu:51024" # replace with your port number
    print("testing for server: " + SITE_URL)
    SONGS_URL = SITE_URL + "/songs/"
    RESET_URL = SITE_URL + "/reset/"

    ####################From test-endpoint#################################
    def reset_data(self):
        s = {}
        r = requests.put(self.RESET_URL, data = json.dumps(s))


    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_songs_get(self):
        self.reset_data()
        song_id = 1
        r = requests.get(self.SONGS_URL + str(song_id))
        self.assertTrue(self.is_json(r.content.decode("utf-8")))
        resp = json.loads(r.content.decode("utf-8"))
        self.assertEqual(resp["title"], "1904")
        self.assertEqual(resp["artist"], "The Tallest Man on Earth")
        self.assertEqual(resp["year"], "2012")

    def test_songs_put(self):
        self.reset_data()
        song_id = 2

        r = requests.get(self.SONGS_URL + str(song_id))
        self.assertTrue(self.is_json(r.content.decode("utf-8")))
        resp = json.loads(r.content.decode("utf-8"))
        self.assertEqual(resp["title"], "#40")
        self.assertEqual(resp["artist"], "Dave Matthews")
        self.assertEqual(resp["year"], "1999")

        s = {}
        s["title"] = "ABC"
        s["artist"] = "DEF"
        s["year"] = "2020"
        r = requests.put(self.SONGS_URL + str(song_id), data = json.dumps(s))
        self.assertTrue(self.is_json(r.content.decode("utf-8")))
        resp = json.loads(r.content.decode("utf-8"))
        self.assertEqual(resp["result"], "success")

        r = requests.get(self.SONGS_URL + str(song_id))
        self.assertTrue(self.is_json(r.content.decode("utf-8")))
        resp = json.loads(r.content.decode("utf-8"))
        self.assertEqual(resp["title"], s["title"])
        self.assertEqual(resp["artist"], s["artist"])
        self.assertEqual(resp["year"], s["year"])

    def test_songs_delete(self):
        self.reset_data()
        song_id = 3

        s = {}
        r = requests.delete(self.SONGS_URL + str(song_id), data = json.dumps(s))
        self.assertTrue(self.is_json(r.content.decode("utf-8")))
        resp = json.loads(r.content.decode("utf-8"))
        self.assertEqual(resp["result"], "success")

        r = requests.get(self.SONGS_URL + str(song_id))
        self.assertTrue(self.is_json(r.content.decode("utf-8")))
        resp = json.loads(r.content.decode("utf-8"))
        self.assertEqual(resp["result"], "error")
        #self.assertEqual(resp["message"], "movie not found")

    ##################from test-del-only#########################
    """def reset_data(self):
        s = {}
        r = requests.put(self.RESET_URL, data = json.dumps(s))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False"""

    def test_songs_delete(self):
        self.reset_data()
        song_id = 30

        s = {}
        r = requests.delete(self.SONGS_URL + str(song_id), data = json.dumps(s))
        self.assertTrue(self.is_json(r.content))
        resp = json.loads(r.content)
        self.assertEqual(resp["result"], "success")

        r = requests.get(self.SONGS_URL + str(song_id))
        self.assertTrue(self.is_json(r.content))
        resp = json.loads(r.content)
        self.assertEqual(resp["result"], "error")

    ######################from test-index##############################
    def test_songs_index_get(self):
        self.reset_data()
        r = requests.get(self.SONGS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testsong = {}
        songs = resp["songs"]
        for song in songs:
            if song["id"] == 1:
                testsong = song

        self.assertEqual(testsong["title"], "1904")
        self.assertEqual(testsong["artist"], "The Tallest Man on Earth")
        self.assertEqual(testsong["year"], "2012")

    def test_songs_index_post(self):
        self.reset_data()

        s = {}
        s["title"] = "ABC"
        s["artist"] = "DEF"
        s["year"] = "2020"
        r = requests.post(self.SONGS_URL, data = json.dumps(s))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp["result"], "success")
        self.assertEqual(resp["id"], 129) # may fail

        r = requests.get(self.SONGS_URL + str(resp["id"]))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp["title"], s["title"])
        self.assertEqual(resp["artist"], s["artist"])
        self.assertEqual(resp["year"], s["year"])

    def test_songs_index_delete(self):
        self.reset_data()

        s = {}
        r = requests.delete(self.SONGS_URL, data = json.dumps(s))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp["result"], "success")

        r = requests.get(self.SONGS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        songs = resp["songs"]
        self.assertFalse(songs)

    ##################from test-reset##########################
    def test_reset_data(self):
        s = {}
        r = requests.put(self.RESET_URL)

if __name__ == "__main__":
    unittest.main()

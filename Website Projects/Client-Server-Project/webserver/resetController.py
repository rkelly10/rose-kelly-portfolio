import cherrypy
import re, json
from songs_library import _song_database

class ResetController(object):

    def __init__(self, sdb=None):
        if sdb is None:
            self.sdb = _song_database()
        else:
            self.sdb = sdb


    def PUT_INDEX(self):
        output = {'result':'success'}

        data = json.loads(cherrypy.request.body.read().decode())

        self.sdb.__init__()
        self.sdb.load_songs('songlist.json')

        return json.dumps(output)

    def PUT_KEY(self, song_id):
        output = {'result':'success'}
        mid = int(song_id)

        try:
            data = json.loads(cherrypy.request.body.read().decode())

            sdbtmp = _song_database()
            sdbtmp.load_songs('songlist.json')

            song = sdbtmp.get_song(mid)

            self.sdb.set_song(mid, song) #remember to reset genre also


        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

## Server

*See main README.md file for how to test*

## RESTful JSON spec

| COMMAND | RESOURCE | Input Example | Output Example |
| ------ | ------ | ------ | ------ |
| GET | /songs/ |  | {"result":"success", "songs": [{"title":..},{...},...]} |
| POST | /songs/ | {"title": "Daily Growing", "artist": "Altan", "year": "2002"} | {"result":"success", "id": 129} |
| DELETE | /songs/ |  | {"result":"success"} |
| GET | /songs/:song_id |  | {"title": "1904", "artist": "The Tallest Man on Earth", "year": "2012"} |
| PUT | /songs/:song_id | {"result":"success"} |
| DELETE | /songs/:song_id |  | {"result":"success"} |
| PUT | /reset/ | | {"result":"success"} |
| PUT | /reset/:song_id | | {"result":"success"} |

## Port # / description

Our port number is 51024.

Our server uses a .json file to load a playlist of songs (with the data fields of id, title, artist, and year). The server will allow a client to access the data fields of title, artist, and year for each song OR for all songs in the list using a GET request. Clients can also clear the list or remove an individual song with a DELETE request. They can add songs to the playlist by sending a POST request to the server.


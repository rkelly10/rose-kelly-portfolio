import json

class _song_database:

       def __init__(self):
        self.song_titles = dict()
        self.song_artists = dict()
        self.song_years = dict()

       def load_songs(self, song_file):
        with open(song_file, "r") as f:
            song_dict = json.load(f)
        sid = 1
        for s in song_dict["songs"]:
            self.song_titles[sid] = s["title"]
            self.song_artists[sid] = s["artist"]
            self.song_years[sid] = s["year"]
            sid = sid + 1


       def get_songs(self):
        return self.song_titles.keys()

       def get_song(self, sid):
        try:
                stitle = self.song_titles[sid]
                sartists = self.song_artists[sid]
                syears = self.song_years[sid]
                song = list((stitle, sartists, syears))
        except Exception as ex:
                song = None
        return song

       def set_song(self, sid, song):
        self.song_titles[sid] = song[0]
        self.song_artists[sid] = song[1]
        self.song_years[sid] = song[2]

       def delete_song(self, sid):
        del(self.song_titles[sid])
        del(self.song_artists[sid])
        del(self.song_years[sid])



if __name__ == "__main__":
       sdb = _song_database()

       #### MOVIES ########
       sdb.load_songs("songlist.json")

       song = sdb.get_song(8)
       print(song[0])

       song[0] = "ABC"
       sdb.set_song(2, song)

       print("A")
       print("B")

       song = sdb.get_song(2)
       print(song[0])
       ####################

       ####################

## OO Library API 

*See main README.md file for how to test*

<<<<<<< HEAD
=======
| COMMAND | RESOURCE | Input Example | Output Example |
| ------ | ------ | ------ | ------ |
| GET | /songs/ |  | {"result":"success", "songs": [{"title":..},{...},...]} |
| POST | /songs/ | {"title": "Daily Growing", "artist": "Altan", "year": "2002"} | {"result":"success", "id": 129} |
| DELETE | /songs/ |  | {"result":"success"} |
| GET | /songs/:song_id |  | {"title": "1904", "artist": "The Tallest Man on Earth", "year": "2012"} |
| PUT | /songs/:song_id |  | {"result":"success"} |
| DELETE | /songs/:song_id |  | {"result":"success"} |
| PUT | /reset/ | | {"result":"success"} |
| PUT | /reset/:song_id | | {"result":"success"} |


>>>>>>> 95ab54dc3f10ad55637e772c77dff08af0681edb
API library files are songs_library.py and songlist.json. These are used to create a list of songs (with id, title, artist, year) for the song database.


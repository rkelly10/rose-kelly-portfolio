import unittest
import requests
import json
from songs_library import _song_database

class TestMoviesIndex(unittest.TestCase):
    def can_load_data(self):
        try:
            sdb = _song_database()
            sdb.load_songs('songlist.json')
            return True
        except Exception as ex:
            return False

    def test_get_song(self):
        sdb = _song_database()
        sdb.load_songs('songlist.json')
        song = sdb.get_song(2)
        self.assertEqual(song[0], '#40')
        self.assertEqual(song[2], '1999')

    def test_get_songs(self):
        sdb = _song_database()
        sdb.load_songs('songlist.json')
        keys = list(sdb.get_songs())
        self.assertEqual(len(keys), 128)



    def test_set_song(self):
        sdb = _song_database()
        sdb.load_songs('songlist.json')
        song = sdb.get_song(8)
        song[0] = 'ABC'
        sdb.set_song(8, song)
        song2 = sdb.get_song(8)
        self.assertEqual(song2[0], 'ABC')

    def test_delete_song(self):
        sdb = _song_database()
        sdb.load_songs('songlist.json')
        sdb.delete_song(76)
        keys = list(sdb.get_songs())
        self.assertEqual(len(keys), 127)



if __name__ == "__main__":
    unittest.main()


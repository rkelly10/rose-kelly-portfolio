## Paradigms-Client-Server

Frank Gomulka (fgomulka)
Gehrig Chao (gchao2)
Rose Kelly (rkelly10)

## IMPORTANT NOTE TO PORTFOLIO VIEWERS

This client-server system was built around running the server using the university's url and an assigned port. However since this class is taught every semester, access to these resources is restricted after the semester ends so the next class can use them. As such, the server can no longer be run in the manner described below.


## Playlist API

Our project uses a json file filled with popular songs, and these songs are then added to the user’s playlist. The data for each song are: title, artist, year. The playlist can also be cleared, searched (by song id), and have individual songs removed from it.

## Tests

First, run `python3 server.py` in one terminal. 

Our OO library can be tested by running `python3 test_api.py`

Our server can be tested by running `python3 test_ws.py`

## OO Library API 

See above for how to test.

| COMMAND | RESOURCE | Input Example | Output Example |
| ------ | ------ | ------ | ------ |
| GET | /songs/ |  | {"result":"success", "songs": [{"title":..},{...},...]} |
| POST | /songs/ | {"title": "Daily Growing", "artist": "Altan", "year": "2002"} | {"result":"success", "id": 129} |
| DELETE | /songs/ |  | {"result":"success"} |
| GET | /songs/:song_id |  | {"title": "1904", "artist": "The Tallest Man on Earth", "year": "2012"} |
| PUT | /songs/:song_id |  | {"result":"success"} |
| DELETE | /songs/:song_id |  | {"result":"success"} |
| PUT | /reset/ | | {"result":"success"} |
| PUT | /reset/:song_id | | {"result":"success"} |


API library files are songs_library.py and songlist.json. These are used to create a list of songs (with id, title, artist, year) for the song database.

## Server

See above for how to test.
Our port number is 51024.
Our server uses a .json file to load a playlist of songs (with the data fields of id, title, artist, and year). The server will allow a client to access the data fields of title, artist, and year for each song OR for all songs in the list using a GET request. Clients can also clear the list or remove an individual song with a DELETE request. They can add songs to the playlist by sending a POST request to the server.

## Running Project Code for Web Client Use

First, run `python3 server.py` in one terminal. 

Now that the server is running, open the html file for the front end, 'playlist_editor.html', in a browser (double click on file on PM OR follow the instructions to host the client on student04)

    [note: the js file request-sending-script.js must be in a folder titled scripts in the same directory as the html file for the client to function]

To use the front end, you may first display a playlist that has already been populated using the 'Display Playlist' button [note: you may choose the way that the displayed playlist is sorted.

To add a song, enter its name, artist and year in the corresponding input boxes and click on the 'Add Song' button

To delete a song, enter its song id in the input box and click the 'Delete Song' button

To display the songs in a playlist, select what order you would like to see them in by clicking its corresponding radio button and then click the 'Display Playlist' button

To delete all songs in a playlist, click the 'Delete Playlist' button

To reset a playlist to its original song contents, click the 'Reset Playlist' button

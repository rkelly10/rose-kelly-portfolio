Created in CSE 40793 Software Development Practices with fellow students Matt Kozak, Sean Crotty and Justin Virgadamo.

# Running Away From HERE

Since this website is not currently hosted anywhere, you will need to run it on your own computer to view it.

To build, you'll need flask and flask_mysqldb. To install both, simply run ```pip install flask``` and ```pip install flask_mysqldb```.

From there, export your flask app using export FLASK_APP=run.py.

Finally, run the server by running flask run (after navigating to inside of the Running-Away-From-Here folder)



I have also attached some images of our website below for easier viewing.

![Login Screen][login]

[login]: img/login.png "Login Screen"


![Home Screen][home]

[home]: img/home.png "Home Screen"


![Trail Descriptions][screen1]

[screen1]: img/trail_descriptions.png "Trail Descriptions"


from flask import render_template, jsonify, session, request, redirect, url_for
from app import app
from flask_mysqldb import MySQL
from .api import *

app.config.from_pyfile('config.py')

db = MySQL(app)

# PAGES
@app.route('/')
def home():
    session['logged_in'] = False
    return render_template('login.html', title='Login')

@app.route('/index')
def index():
    if session['logged_in']:
        return render_template('index.html', title='Home')
    return redirect('/login')

@app.route('/display_trails')
def display_trails():
    if session['logged_in']:
        return render_template('display_trails.html', title='Trail Descriptions')
    return redirect('/login')

@app.route('/time_run')
def time_run():
    if session['logged_in']:
        return render_template('time_run.html', title='Time Run')
    return redirect('/login')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if session['logged_in']:
        return render_template('index.html', title='Home')
    return render_template('login.html', title='Login')

@app.route('/leaderboard')
def leaderboard():
    if session['logged_in']:
        return render_template('leaderboard.html', title='Leaderboard')
    return render_template('login.html', title='Login')

@app.route('/friends')
def friends():
    if session['logged_in']:
        return render_template('friends.html', title='View Friends')
    return render_template('login.html', title='Login')

@app.route('/add_friends')
def addFriends():
    if session['logged_in']:
        return render_template('add_friends.html', title='Add Friends')
    return render_template('login.html', title='Login')

# METHODS
@app.route('/getTrailDescriptions', methods=['GET', 'POST'])
def postGetTrailDescriptions():
    trailDescriptions = getTrailDescriptions(db)
    data = { "trailDescriptions": trailDescriptions }
    result = {str(key): value for key, value in data.items()}
    return jsonify(result=result)

@app.route('/getUsers', methods=['GET', 'POST'])
def postgetUsers():
    userNames = getUsers(db)
    data = { "userNames": userNames }
    result = {str(key): value for key, value in data.items()}
    return jsonify(result=result)

@app.route('/addFriend', methods=['GET', 'POST'])
def postAddFriend():
    friendName = request.form.get("friendName")
    success = addFriend(db, friendName, session['username'])
    return jsonify(result=success)

@app.route('/addWorkout', methods=['GET', 'POST'])
def postAddWorkout():
    timeString = request.form.get("trailTime")
    route = request.form.get("trailName")
    success = addWorkout(db, timeString, route, session['username'])
    return jsonify(result=success)

@app.route('/getLeaderboardForTrail', methods=['GET', 'POST'])
def postGetLeaderboardForTrail():
    trailName = request.form.get("trailName")
    leaderboard = getLeaderboardForTrail(db, trailName)
    data = { "leaderboard": leaderboard }
    result = {str(key): value for key, value in data.items()}
    return jsonify(result=result)

@app.route('/userLogin', methods=['GET', 'POST'])
def postUserLogin():
    username = request.form.get("username")
    password = request.form.get("password")
    verified = userLogin(db, username, password)
    data = {}
    if verified:
        session['logged_in'] = True
        session['username'] = username
        data['verified'] = True
        result = {str(key): value for key, value in data.items()}
        return jsonify(result=result)
    data['verified'] = False
    result = {str(key): value for key, value in data.items()}
    return jsonify(result=result)

from flask_mysqldb import MySQL
import json

def getTrailDescriptions(db):
    cur = db.connection.cursor()
    cur.execute('''SELECT name, distance, description FROM routes''')
    results = cur.fetchall()
    return str(json.dumps(results))

def addWorkout(db, timeString, route, username):
    cur = db.connection.cursor()
    cur.execute("INSERT INTO workouts (id, route_id, time, user) VALUES (NULL, (SELECT id FROM routes WHERE name=%s), %s, (SELECT id FROM users WHERE username=%s))", (route, timeString, username))
    db.connection.commit()
    results = cur.fetchall()
    return str(json.dumps(results))

def addFriend(db, friendName, username):
    cur = db.connection.cursor()
    cur.execute("INSERT INTO friends (id, user, friend) VALUES (NULL, (SELECT id FROM users WHERE username=%s), (SELECT id FROM users WHERE username=%s))", (username, friendName))
    db.connection.commit()
    results = cur.fetchall()
    return str(json.dumps(results))


def getLeaderboardForTrail(db, trailName):
    cur = db.connection.cursor()
    cur.execute("SELECT username, MIN(time) AS time FROM users, workouts WHERE workouts.user=users.id AND route_id=(SELECT id FROM routes WHERE name=%s) GROUP BY username ORDER BY MIN(time) ASC", (trailName,))
    results = cur.fetchall()
    for obj in results:
        obj['time'] = str(obj['time'])
    return str(json.dumps(results))

def getUsers(db):
    cur = db.connection.cursor()
    cur.execute('''SELECT username, id FROM users''')
    results = cur.fetchall()
    return str(json.dumps(results))

def userLogin(db, username, password):
    cur = db.connection.cursor()
    cur.execute("SELECT id FROM users WHERE username=%s AND password=%s", (username, password))
    results = cur.fetchall()
    if (len(results)):
        return True
    return False

function getUsers(){
  $.ajax({
    url: "/getUsers",
    type: "POST",
    data: {}
  }).done(function(response) {
    var data = JSON.parse(response.result.userNames);
    var html = '';
    for (obj in data){
      html += '<tr>';
      html += '<td>' + data[obj].username + '</td>';
      html += '</tr>';
      $('#users').append(html);
      html = '';
    }
  });
}


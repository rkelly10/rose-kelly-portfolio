function addFriend(){
    var friendName = $('#friendName').val();
    $.ajax({
        url: "/addFriend",
        type: "POST",
        data: {friendName: friendName}
    });
}

let addButton = document.getElementById("add-friend-button");

addButton.addEventListener("click", function(){
    alert("Friend Added");
});

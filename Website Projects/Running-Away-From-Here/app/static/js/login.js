function userLogin(){
    var username = $('#username').val();
    var password = $('#password').val();
    $.ajax({
      url: "/userLogin",
      type: "POST",
      data: {username: username, password: password}
    }).done(function(response) {
      var verified = JSON.parse(response.result.verified);
      if (verified == true) {
        window.location.replace("index");
      } else {
        window.location.replace("login");
      }
    });
}

var username = document.getElementById('username');
var password = document.getElementById('password');

var username_error = document.getElementById('username_error');
var pass_error = document.getElementById('pass_error');

username.addEventListener('textInput', usernameVerify);
password.addEventListener('textInput', passVerify);

function validated(){
    if (username.value.length < 6) {
        username.style.border = "1px solid red";
        username_error.style.display = "block";
        username.focus();
        return false;
    }
    if (password.value.length < 6) {
        password.style.border = "1px solid red";
        pass_error.style.display = "block";
        password.focus();
        return false;
    }

}
function usernameVerify(){
    if (username.value.length >= 6) {
        username.style.border = "1px solid silver";
        username_error.style.display = "none";
        return true;
    }
}
function passVerify(){
    if (password.value.length >= 9) {
        password.style.border = "1px solid silver";
        pass_error.style.display = "none";
        return true;
    }
}

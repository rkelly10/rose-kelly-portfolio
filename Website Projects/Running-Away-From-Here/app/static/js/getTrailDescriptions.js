function getTrailDescriptions(){
  $.ajax({
    url: "/getTrailDescriptions",
    type: "POST",
    data: {}
  }).done(function(response) {
    var data = JSON.parse(response.result.trailDescriptions);
    var html = '';
    for (obj in data){
      html += '<tr>';
      html += '<td>' + data[obj].name + '</td>';
      html += '<td>' + data[obj].distance + '</td>';
      html += '<td>' + data[obj].description + '</td>';
      html += '</tr>';
      $('#trailDescriptions').append(html);
      html = '';
    }
  });
}

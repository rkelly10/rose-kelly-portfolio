function getLeaderboardForTrail(){
  var trailName = $('#trailName').val();
  $.ajax({
    url: "/getLeaderboardForTrail",
    type: "POST",
    data: { trailName: trailName }
  }).done(function(response) {
    var data = JSON.parse(response.result.leaderboard);
    buildTableHeader();
    var html = '';
    for (obj in data){
      html += '<tr>';
      html += '<td>' + trailName + '</td>';
      html += '<td>' + data[obj].username  + '</td>';
      html += '<td>' + data[obj].time + '</td>';
      html += '</tr>';
      $('#leaderboard').append(html);
      html = '';
    }
  });
}

function buildTableHeader(){
  var html = '<tr>';
  html += '<th> TRAIL NAME </th>';
  html += '<th> USERNAME </th>';
  html += '<th> RUN TIME </th>';
  html += '</tr>';
  $('#leaderboard').html(html);
}

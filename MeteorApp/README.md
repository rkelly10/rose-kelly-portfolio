Created in CSE30332: Programming Paradigms with fellow students Annie Hermann and Jack Bigej

## How to Run the Program

The easiest way to view this application currently is through Android Studio. If you already have Android Studio, it is simply a matter of opening the MeteorApp folder as a project in Android Studio and running it on an emulator.

If you do not already have Android Studio, you may be in for a long, bulky download if you want to view it that way. However, I have also provided some images below of the interface that give you an idea of how the Meteor Information app would be used.

![Opening Screen][screen1]

[screen1]: img/meteor_app.png "Opening Screen"

From this screen you simply follow the directions given in order to find your desired information. Clicking search with nothing entered will give you a list of all meteors in NASA's database. You can also enter a year or the name of a meteor and it will give you information as shown on the screen below.

![Search Results Screen][screen2]

[screen2]: img/search.png "Search Results"
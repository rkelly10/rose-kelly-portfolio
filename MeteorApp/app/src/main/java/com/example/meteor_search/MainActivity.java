package com.example.meteor_search;

import androidx.appcompat.app.AppCompatActivity;

import com.example.meteor_search.Utilities.Meteor;
import com.example.meteor_search.Utilities.NetworkUtilities;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.view.Menu;
import android.widget.Toast;

import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private EditText mSearchBoxEditText;
    private TextView mDisplayTextView;
    private TextView mSearchResultsTextView;
    private String searchTerm = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // connect layout elements to local variables
        mSearchBoxEditText = (EditText) findViewById(R.id.et_search_box);
        mDisplayTextView = (TextView) findViewById(R.id.tv_display_text);
        mSearchResultsTextView = (TextView) findViewById(R.id.tv_results);



    } // end of onCreate

    //Network Code Begins

    public void makeAsteroidNetworkSearchQuery(){
        // get the search string
        String searchTerm = mSearchBoxEditText.getText().toString();
        //reset the search results
        mSearchResultsTextView.setText("\n\nResults for " + searchTerm + ": ");
        mDisplayTextView.setText("");
        // make the search - network
        new FetchNetworkData().execute(searchTerm);
    } // end of makeQuery


    public class FetchNetworkData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params){
            //get the search term
            if(params.length != 0){
                searchTerm = params[0];
            }

                // get the Url
            URL searchUrl = NetworkUtilities.buildMeteorsUrl();
                // get the response from the URl
            String responseString = null;
            try{
                responseString = NetworkUtilities.getResponseFromUrl(searchUrl); // TODO write this method
            }catch(Exception e){
                e.printStackTrace();
            }
            return responseString;
        } // end of doInBackground

        @Override
        protected void onPostExecute(String responseData){
            Meteor [] meteors = NetworkUtilities.parseMeteorsJson(responseData);

        // display entries in GUI
            //displays all meteors we have data on if user enters nothing
            if(searchTerm.equals("")) {
                for(Meteor meteor: meteors){
                    if(meteor != null) {
                        String mName = meteor.getName();

                        mSearchResultsTextView.append("\n\n" + mName );
                    }
                }
            }
            else {
                boolean found = false;
                //if search term is entered, goes through all meteors looking for matches and displays those matches
                for(Meteor meteor: meteors){
                    if(meteor != null && (searchTerm.equals(meteor.getName()) || searchTerm.equals(meteor.getDate()) )) {
                        String mName = meteor.getName();
                        found = true;
                        mSearchResultsTextView.append("\n\n" + mName + "\n    Mass: " + meteor.getMass() + " kg\n    Landing Date: " + meteor.getDate() );
                    }
                }
                // if no matches found, tells user
                if(!found) {
                    mSearchResultsTextView.append("\n\nNo results match " + searchTerm +".");
                }
            }

        } // end of onPost
    } // end of inner class

    //Networking related code ends


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    } // end of onCreateOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemSelected = item.getItemId();

        // if search option was selected
        if(menuItemSelected == R.id.action_search) {
            Context context = MainActivity.this;
            String message = "Search selected";
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            makeAsteroidNetworkSearchQuery();
        } // end of if
        return true;
    } // end of onOptionsItemSelected

} // end of MainActivity
package com.example.meteor_search.Utilities;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;



public class NetworkUtilities {

    // this method is specific to the countries URL
    public static URL buildMeteorsUrl(){
        // get string url
        String meteorUrlString = "https://data.nasa.gov/resource/y77d-th95.json";
        URL meteorUrl = null;
        try{
            meteorUrl = new URL(meteorUrlString);
        }catch(MalformedURLException e){
            e.printStackTrace();
        }
        return meteorUrl;
    } // end of build

    // this method can be used with any URL object
    public static String getResponseFromUrl(URL url) throws IOException{
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection(); // getting the connection open
        try{
            InputStream in = urlConnection.getInputStream();
            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A"); // delimiter for end of message
            boolean hasInput = scanner.hasNext();
            if(hasInput) return scanner.next(); // success
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            urlConnection.disconnect();
        }
        return null;
    } // end of get Resp

    // Function parses meteor json to make array of meteors
    public static Meteor [] parseMeteorsJson(String meteorResponseString){
        //creates empty array of meteors
        Meteor[] meteorList = new Meteor[4096];
        try{
            //makes json array out of json text
            JSONArray meteorsArray = new JSONArray(meteorResponseString);

            int j = 0;
            //goes through each meteor in jsonarray
            for (int i = 0; i < meteorsArray.length(); i++) {

                try {
                    //attempts to parse information from each meteor in list
                    Object item = meteorsArray.get(i);
                    JSONObject jsonitem = (JSONObject) item;

                    String name = (String) jsonitem.get("name");
                    String mass = (String) jsonitem.get("mass");
                    String date = (String) jsonitem.get("year");
                    
                    //if all information necessary to make a meteor (mass, name and date) are available, makes a meteor
                    if (name != null && mass != null && date != null) {
                        Meteor m = new Meteor(name, Double.valueOf(mass), date);
                        meteorList[j] = m;
                        j++;
                    }
                    // if not, moves to next meteor
                } catch(JSONException e){

                }
            }


        } catch(JSONException e){
            e.printStackTrace();
        }
        return meteorList;
    } // end of parse


} // end of class

package com.example.meteor_search.Utilities;

public class Meteor {
    String name;
    String date;
    double mass;

    Meteor() {
        this.name = "";
        this.mass = 0;
        this.date = "";
    }

    Meteor(String name, double mass, String date) {
        this.name = name;
        this.mass = mass;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public double getMass() {
        return mass;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setName(Double mass) {
        this.mass = mass;
    }

} // end of Meteor class

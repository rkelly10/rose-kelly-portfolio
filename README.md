## Welcome to My Portfolio!

Here you will be able to find some projects I have worked on either in class or in my free time. 

Some of the highlighted projects I have chosen for this portfolio include:

        Running Away From Here: A website made for runners to be able to track their times on local South Bend trails and
         compare said times with other runners on the website's leaderboard. Built with Python and Flask in the Fall 2020 
         semester

        City Information Scraper: A Python GUI that allows users to enter any United States city and recieve information 
        about the area like crime rates, the weather and a list of local sex offenders. Built using web scraping and requires
        internet accesss to run.

        Meteor Information App: An Android app that allows users to access NASA's database on meteors to find info about any
         meteor ever found and recorded. Built with Java and Android studio.


To find out more about these projects you can navigate to their folders and look at their README's, which have screenshots and more details about the programs.